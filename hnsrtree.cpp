#define BUILDING_NODE_EXTENSION

#include <node.h>
#include <v8.h>
#include <vector>
#include "hnsrtree.h"
#include "HnSRTree/HnSRTreeFile.hh"

using namespace v8;

Persistent<Function> HnSRIndex::constructor;

// Init object exports
void HnSRIndex::Init( Handle<Object> exports ) {
	// Export object
	Local<FunctionTemplate> tpl = FunctionTemplate::New(New);
	tpl->SetClassName(String::NewSymbol("HnSRIndex"));
	tpl->InstanceTemplate()->SetInternalFieldCount(1);

	//Prototype
	tpl->PrototypeTemplate()->Set(String::NewSymbol("getNeighbors"),
		FunctionTemplate::New(GetNeighbors)->GetFunction());

	constructor = Persistent<Function>::New(tpl->GetFunction());

	exports->Set(String::NewSymbol("HnSRIndex"), constructor);
};

HnSRIndex::HnSRIndex( const char* filename ) {
	this->filename = new char[strlen(filename)+1];
	strcpy( this->filename, filename );

	indexFile = new_HnSRTreeFile( this->filename, "r" );

	if( indexFile == HnSRTreeFile::null )
		ThrowException(Exception::TypeError(String::New("Could not open index file.")));
};

// JavaScript constructor
v8::Handle<v8::Value> HnSRIndex::New(const v8::Arguments& args) {
	HandleScope scope;

	// Not called as a constructor.
	if( !args.IsConstructCall() ) {
		ThrowException(Exception::TypeError(String::New("This method is a constructor.")));
		return scope.Close(Undefined());
	}

	// Constructor call, verify parameters.
	if( args.Length() != 1 ) {
		ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
		return scope.Close(Undefined());
	}

	if( !args[0]->IsString() ) {
		ThrowException(Exception::TypeError(String::New("First argument should be the filename of the index file.")));
		return scope.Close(Undefined());
	}

	// Retrieve C string
	v8::String::Utf8Value filename(args[0]->ToString());

	// Create new instance and wrap it
	HnSRIndex* obj = new HnSRIndex( *filename );
	obj->Wrap(args.This());

	return scope.Close(args.This());
};

// GetNeighbors forward
v8::Handle<v8::Value> HnSRIndex::GetNeighbors(const v8::Arguments& args) {
	HandleScope scope;

	// Verify parameters
	if( args.Length() < 1 || args.Length() > 2 ) {
		ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
		return scope.Close(Undefined());
	}

	if( !args[0]->IsArray() ) {
		ThrowException(Exception::TypeError(String::New("First argument should be the reduced sequence.")));
		return scope.Close(Undefined());
	}

	if( !args[1]->IsNumber() ) {
		ThrowException(Exception::TypeError(String::New("Second argument should be the number of neighbors to retrieve.")));
		return scope.Close(Undefined());
	}

	// The number of neighbors to retrieve
	int numNeighbors = (int)(args[1]->NumberValue());

	HnSRIndex* self = ObjectWrap::Unwrap<HnSRIndex>(args.This());
		
        HnPoint queryPoint;
        int i;
        HnPointVector points;
        HnDataItemVector dataItems;
		
		/*Initialiseer de vector met alle waarden coordinaat waardes*/
		std::vector<double> vector;
		Local<Array> inputArr = Array::Cast(*args[0]);
		for( uint32_t i=0; i<inputArr->Length(); i++ ) {
			vector.push_back( inputArr->Get(i)->NumberValue() );
		}
		
		/*Initialiseer querypoint met de waarden van elke dimensie*/
		queryPoint = new_HnPoint(self->indexFile.getDimension());
		
		for(int i = 0; i < self->indexFile.getDimension(); i++)
		{
			queryPoint.setCoordAt(vector[i], i);
		}

        /* open an index file and prepare `queryPoint' and `numNeighbors' */

		/*Get nearest neighbour keys in dataItems*/
        self->indexFile.getNeighbors(queryPoint, numNeighbors, &points, &dataItems);

		/*Zet alle nearest neighbours keys in arr*/
			Handle<Array> arr = Array::New(points.size());
        for ( i=0; i<points.size(); i++ ) {
            HnPoint point = points.elementAt(i);
            HnDataItem dataItem = dataItems.elementAt(i);

            /* work with `point' and `dataItem' */
			const char *id = dataItem.toCharArray();
			arr->Set(i,String::New( id, strlen(id)));
        }
		
	// Return the correct response
	return scope.Close(arr);
};

// Module initialization
void init(Handle<Object> exports) {
	// Export Normalize
	exports->Set( String::NewSymbol("version"),
		String::New("0.1.0"));

	HnSRIndex::Init(exports);
}

NODE_MODULE(hnsrtree, init)