
/*
 * HnPointObj.cc
 * Copyright (C) 1997 Norio Katayama
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA
 *
 * 06/29/96 katayama@rd.nacsis.ac.jp
 * $Id: HnPointObj.cc,v 1.7 2000/12/15 10:01:12 katayama Exp $
 */

#include "HnSRTree/HnPoint.hh"
#include "HnSRTree/HnPointObj.hh"
#include "HnSRTree/HnFPControl.h"

#include <cmath>        // for std::pow, std::sqrt
#include <functional>   // for std::less
#include <iterator>     // for std::next

#define kMindistR 5

inline double* begin(HnPointObj& self)
{
  return self.getCoords();
}

inline const double* begin(const HnPointObj& self)
{
  return self.getCoords();
}

inline double* end(HnPointObj& self)
{
  return begin(self) + self.getDimension();//std::next(begin(self), self.getDimension());
}

inline const double* end(const HnPointObj& self)
{
  return begin(self) + self.getDimension();//std::next(begin(self), self.getDimension());
}

namespace dtw
{

class Enveloppe
{
public:
  typedef HnPointObj Sequence;
  typedef std::size_t size_type;

public:
  Enveloppe(Sequence min, Sequence max)
  : min_(min)
  , max_(max)
  { }

  Sequence& min() { return min_; }
  Sequence& max() { return max_; }

  const Sequence& min() const { return min_; }
  const Sequence& max() const { return max_; }

  size_type size() const { return min_.getDimension(); }

private:
  Sequence min_;
  Sequence max_;
};

template<typename T>
struct identity { typedef T type; };

namespace util
{

template<typename T, typename Pred>
inline T clamp(T x,
               typename identity<T>::type lo,
               typename identity<T>::type hi,
               Pred p)
{
  return p(x, lo) ? lo : p(hi, x) ? hi : x;
}

template<typename T>
inline T clamp(T x,
               typename identity<T>::type lo,
               typename identity<T>::type hi)
{
  return clamp(x, lo, hi, std::less<T>());
}

}

namespace algo
{

class keogh
{
public:
  typedef keogh this_type;

public:
  keogh(std::size_t r)
  : r_(r)
  { }

  double operator()(const HnPointObj& Q, const HnPointObj& C) const
  {
    typedef double value_type;
    typedef int size_type;

    double total = 0;
    for(size_type i = 0; i < C.getDimension(); ++i) {
      const value_type LQ = this_type::lower(Q, i, r_);
      const value_type UQ = this_type::upper(Q, i, r_);
      if(C.getCoordAt(i) > UQ) {
        total += std::pow((C.getCoordAt(i) - UQ), 2);
      } else if(C.getCoordAt(i) < LQ) {
        total += std::pow((C.getCoordAt(i) - LQ), 2);
      } else {
        // total += 0
      }
    }
    return std::sqrt(total);
  }

public:
  static double lower(const HnPointObj& Q, int i, int r)
  {
    const int idx_min = util::clamp(i - r, 0, Q.getDimension());
    const int idx_max = util::clamp(i + r, 0, Q.getDimension());

    if( idx_min == idx_max ) return *begin(Q);
    double smallest = *(begin(Q) + idx_min);

    for( double* i=(double*)(begin(Q) + idx_min); i < begin(Q) + idx_max + 1; i++ )
        if( *i < smallest )
            smallest = *i;

    return smallest;
    /*return *std::min_element(begin(Q) + idx_min,
                             begin(Q) + idx_max);*/
  }

  static double upper(const HnPointObj& Q, int i, int r)
  {
    //using std::begin;
    const int idx_min = util::clamp(i - r, 0, Q.getDimension());
    const int idx_max = util::clamp(i + r, 0, Q.getDimension());

    if( idx_min == idx_max ) return *begin(Q);
    double largest = *(begin(Q) + idx_min);

    for( double* i=(double*)(begin(Q) + idx_min); i < begin(Q) + idx_max + 1; i++ )
        if( *i > largest )
            largest = *i;

    return largest;
    /*return *std::max_element(begin(Q) + idx_min,
                             begin(Q) + idx_max);*/
  }

private:
  const int r_;
};

}

Enveloppe make_enveloppe(const HnPointObj& seq, int r)
{
  HnPointObj min(seq.getDimension());
  HnPointObj max(seq.getDimension());
  for(int i = 0; i < seq.getDimension(); ++i) {
    min.setCoordAt(i, algo::keogh::lower(seq, i, r));
    max.setCoordAt(i, algo::keogh::upper(seq, i, r));
  }
  return Enveloppe(min, max);
}

inline static double mindist(const HnPointObj& Q, const HnPointObj& C, int r)
{
  Enveloppe envQ = make_enveloppe(Q, r);
  Enveloppe envC = make_enveloppe(C, r);

  double sum = 0;
  for(Enveloppe::size_type i = 0; i < envQ.size(); ++i) {
    const double mini = envC.min().getCoordAt(i);
    const double maxi = envC.max().getCoordAt(i);
    const double ui = envQ.max().getCoordAt(i);
    const double li = envQ.min().getCoordAt(i);
    if(mini > ui) {
      sum += std::pow((mini - ui), 2);
    } else if (maxi < li) {
      sum += std::pow((maxi - li), 2);
    } else {
      // total += 0;
    }
  }
  return std::sqrt(sum * (Q.getDimension() / envQ.size()));
}

}

HnBool
HnPointObj::equals(const HnPoint &point) const
{
  /*
   * NOTE:
   *      It is inadequate to compare points by memcmp(),
   *      since the same double values do not necessarily have
   *      the same binary value, e.g., 0 (zero) and -0 (minus zero).
   */

  int i;

#ifdef DEBUGGING
  if ( point.getObject()->dimension != dimension ) {
    HnAbort("mismatch in dimensions.");
  }
#endif

  for ( i=0; i<dimension; i++ ) {
    if ( point.getObject()->coords[i] != coords[i] ) {
      return HnFALSE;
    }
  }

  return HnTRUE;
}

HnString
HnPointObj::toString(void) const
{
  HnString string;
  int i;

  string = "HnPoint[";

  for ( i=0; i<dimension; i++ ) {
    if ( i != 0 ) {
      string += ",";
    }
    string += HnString::valueOf(getCoordAt(i));
  }

  string += "]";

  return string;
}

double
HnPointObj::getDistance(const HnPoint &point) const
{
#ifdef DEBUGGING
  if(point.getDimension() > dimension) {
    HnAbort("Q.size > C.size");
  }
#endif

  int r = (int)(point.getDimension()*0.8); // Set window size to 16% of fragment
  return dtw::mindist(point, *this, r);
}

double
HnPointObj::getSquareDistance(const HnPoint &point) const
{
  int i;
  double dif, sum;

#ifdef DEBUGGING
  if ( point.getDimension() != dimension ) {
    HnAbort("mismatch in dimensions.");
  }
#endif

  sum = 0;
  for ( i=0; i<dimension; i++ ) {
    dif = getCoordAt(i) - point.getCoordAt(i);
    sum += dif * dif;
  }

  return sum;
}

double
HnPointObj::getUpperBoundDistance(const HnPoint &point) const
{
  int flag;
  double distance;

  flag = HnFPControl_setRound(HnFPControl_ROUND_PLUS);

  distance = getDistance(point);

  HnFPControl_setRound(flag);

  return distance;
}

double
HnPointObj::getLowerBoundDistance(const HnPoint &point) const
{
  int flag;
  double distance;

  flag = HnFPControl_setRound(HnFPControl_ROUND_MINUS);

  distance = getDistance(point);

  HnFPControl_setRound(flag);

  return distance;
}
