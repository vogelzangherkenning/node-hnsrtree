#ifndef HNSRINDEX_H
#define HNSRINDEX_H

#include <node.h>
#include <v8.h>
#include "HnSRTree/HnSRTreeFile.hh"

using namespace v8;

class HnSRIndex : public node::ObjectWrap {

	public:
		static void Init( Handle<Object> exports );

	private:
		HnSRTreeFile indexFile;
		char* filename;

		explicit HnSRIndex( const char* filename );

		~HnSRIndex() {
			// @TODO: Close index file
		}

		// Object constructor
		static v8::Handle<v8::Value> New(const v8::Arguments& args);

		// Retrieve neigbors of a sequence
		static v8::Handle<v8::Value> GetNeighbors(const v8::Arguments& args);

		static v8::Persistent<v8::Function> constructor;
};

#endif